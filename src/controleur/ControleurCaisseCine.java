package controleur;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ControleurCaisseCine {
   
    int NbAdultes = 0;
    int NbEtudiants = 0;
    int NbEnfants = 0;
    
    boolean promotion = false;
    
    float montantSoiree = 0;
    float PrixMoyen = 0;
    
    public void calculer(){
        
        float total =7f * NbAdultes + 5.5f * NbEtudiants + 4f * NbEnfants;
        
        if( promotion ) { total=total*0.8f;}
        
        int NbPlaces = NbEtudiants+NbEnfants+NbAdultes;
        
        float prixmoyenP;
        
        if (NbPlaces>0) {prixmoyenP=total/NbPlaces;}else{prixmoyenP=0f;}
        
        setmontantSoiree(total);
        SetPrixMoyen(prixmoyenP);
    }
    
    //<editor-fold defaultstate="collapsed" desc="Code généré">
    public static final String PROP_NBADULTES = "NbAdultes";
    
    public int getNbAdultes() {
        return NbAdultes;
    }
    
    public void setNbAdultes(int NbAdultes) {
        int oldNbAdultes = this.NbAdultes;
        this.NbAdultes = NbAdultes;
        propertyChangeSupport.firePropertyChange(PROP_NBADULTES, oldNbAdultes, NbAdultes);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public static final String PROP_NBETUDIANTS = "NbEtudiants";
    
    public int getNbEtudiants() {
        return NbEtudiants;
    }
    
    public void setNbEtudiants(int NbEtudiants) {
        int oldNbEtudiants = this.NbEtudiants;
        this.NbEtudiants = NbEtudiants;
        propertyChangeSupport.firePropertyChange(PROP_NBETUDIANTS, oldNbEtudiants, NbEtudiants);
    }
    
    public static final String PROP_NBENFANTS = "NbEnfants";
    
    public int getNbEnfants() {
        return NbEnfants;
    }
    
    public void setNbEnfants(int NbEnfants) {
        int oldNbEnfants = this.NbEnfants;
        this.NbEnfants = NbEnfants;
        propertyChangeSupport.firePropertyChange(PROP_NBENFANTS, oldNbEnfants, NbEnfants);
    }

    public static final String PROP_PROMOTION = "promotion";
    
    public boolean isPromotion() {
        return promotion;
    }
    
    public void setPromotion(boolean promotion) {
        boolean oldPromotion = this.promotion;
        this.promotion = promotion;
        propertyChangeSupport.firePropertyChange(PROP_PROMOTION, oldPromotion, promotion);
    }
    
    public static final String PROP_MONTANTSOIREE = "montantSoiree";
    
    public float getMontantSoiree() {
        return montantSoiree;
    }
    
    public void setMontantSoiree(float montantSoiree) {
        float oldMontantSoiree = this.montantSoiree;
        this.montantSoiree = montantSoiree;
        propertyChangeSupport.firePropertyChange(PROP_MONTANTSOIREE, oldMontantSoiree, montantSoiree);
    }
    
    public static final String PROP_PRIXMOYEN = "PrixMoyen";
    
    public float getPrixMoyen() {
        return PrixMoyen;
    }
    
    public void setPrixMoyen(float PrixMoyen) {
        float oldPrixMoyen = this.PrixMoyen;
        this.PrixMoyen = PrixMoyen;
        propertyChangeSupport.firePropertyChange(PROP_PRIXMOYEN, oldPrixMoyen, PrixMoyen);
    }

    private void setmontantSoiree(float total) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void SetPrixMoyen(float prixmoyenP) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
//</editor-fold>
